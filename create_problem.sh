#!/usr/local/bin/bash

# Helper to create problem directory skeleton

# NOTE: Bash 4+ is requiired to run this script (https://apple.stackexchange.com/questions/193411/update-bash-to-version-4-0-on-osx)

print_help () {
echo "Usage $(basename $0) -p | --problem problem-number -n | --name problem-name [-h | --help]"
cat << ARGS
   REQUIRED ARGUMENTS:
      -p | --problem
           The problem number
      -n | --name
           The name of the problem file

    OPTIONAL ARGUMENTS:
      -h | --help
           Prints this and exits
ARGS

}

while [[ $# -gt 0 ]]; do
  case $1 in
    -h|--help)
      print_help
      exit 0
      ;;
    -p|--problem)
      PROBLEM="$2"
      shift
      shift
      ;;
    -n|--name)
      NAME="$2"
      shift
      shift
      ;;
    -*|--*)
      echo "Unknown option $1"
      print_help
      exit 1
      ;;
  esac
done
     
if [[ "$PROBLEM" = "" ||  "$NAME" = "" ]];
then
  print_help
  exit 1
else
  echo "Creating problem-$PROBLEM directory..."
fi

DIR=solutions/problem-"$PROBLEM"

if [ -d "$DIR" ];
then
  echo "Directory $DIR already exists."
  exit 1
fi

mkdir -p "$DIR"

cd "$DIR"

touch README.md dune dune-project test.ml "${NAME}.ml"

echo "(lang dune 1.11)" > dune-project

cat > dune << MULTI
(executable
  (name test)
  (libraries ounit2))
MULTI

cat > test.ml << SCAF
open OUnit2
open ${NAME^}

let tests = "Test suite for TODO" >::: [
  "TODO" >:: (fun _ -> assert_equal TODO ($NAME TODO));
]

let _ = run_test_tt_main tests

SCAF

echo "done."

