(* Problem 20: Remove the Kth Element From a List *)

let rec remove_at n l =
  if n < 0 then l
  else match l with
    | [] -> []
    | x :: xs ->
        if n = 0 then xs
        else x :: remove_at (n - 1) xs

