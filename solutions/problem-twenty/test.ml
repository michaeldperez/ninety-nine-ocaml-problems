open OUnit2
open Remove_at

let tests = "Test suite for remove the kth element from a list" >::: [
  "emtpy" >:: (fun _ -> assert_equal [] (remove_at 1 []));
  "k < 0" >:: (fun _ -> assert_equal ["a"; "b"; "c"] (remove_at (-1) ["a"; "b"; "c"]));
  "k > length" >:: (fun _ -> assert_equal ["a"; "b"; "c"] (remove_at 3 ["a"; "b"; "c"]));
  "k in range" >:: (fun _ -> assert_equal ["a"; "c"; "d"] (remove_at 1 ["a"; "b"; "c"; "d"]));
]

let _ = run_test_tt_main tests

