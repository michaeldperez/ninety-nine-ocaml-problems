(* Problem 04: Length *)

let rec length_aux l n =
  match l with
    | [] -> n
    | _ :: xs -> length_aux xs (n + 1)

let length l = length_aux l 0
