open OUnit2
open Length

let tests = "Test suite for length" >::: [
    "empty" >:: (fun _ -> assert_equal 0 (length []));
    "non-empty" >:: (fun _ -> assert_equal 3 (length ["a"; "b"; "c"]));
]

let _ = run_test_tt_main tests
