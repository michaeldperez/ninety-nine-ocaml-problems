open OUnit2
open Rotate

let tests = "Test suite for rotate a list n places to the left" >::: [
  "empty" >:: (fun _ -> assert_equal [] (rotate [] 9));
  "no-wrap" >:: (fun _ -> assert_equal ["d"; "e"; "f"; "g"; "h"; "a"; "b"; "c"] (rotate ["a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"] 3));
  "wrap" >:: (fun _ -> assert_equal ["c"; "d"; "e"; "f"; "g"; "h"; "a"; "b"] (rotate ["a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"] 10));
]

let _ = run_test_tt_main tests

