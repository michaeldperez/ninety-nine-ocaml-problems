(* Problem 19: Rotate a List N Places to the Left *)

let rotate l n =
  if List.length l = 0 then []
  else
    let rec rotate_aux n acc = function
      | [] ->
          if n = 0 then List.rev acc
          else rotate_aux n [] (List.rev acc)
      | x :: xs as t ->
          if n = 0 then t @ List.rev acc
          else rotate_aux (n - 1) (x :: acc) xs
    in rotate_aux n [] l

