open OUnit2
open Encode

let tests = "Test suite for modified run-length encoding" >::: [
  "empty" >:: (fun _ -> assert_equal [] (encode []));
  "one element" >:: (fun _ -> assert_equal [One "a"] (encode ["a"]));
  "uniique elements" >:: (fun _ -> assert_equal [One "a"; One "b"; One "c"] (encode ["a"; "b"; "c"]));
  "Multi-element" >:: (fun _ -> assert_equal [Many (4, "a"); One "b"; Many (2, "c"); Many (2, "a"); One "d"; Many (4, "e")]
    (encode ["a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e"]));
]

let _ = run_test_tt_main tests

