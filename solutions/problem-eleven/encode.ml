(* Problem 11: Modified Run-Length Encoding *)

type 'a rle =
  | One of 'a
  | Many of int * 'a

let encode l =
  let rec encode_aux count acc = function
    | [] -> acc
    | [x] ->
        if count = 0 then
          (One x) :: acc
        else (Many (count + 1, x)) :: acc
    | x :: (y :: _ as xs) ->
        if x = y then 
          encode_aux (count + 1) acc xs
        else
          if count = 0 then
            encode_aux 0 ((One x) :: acc) xs
         else 
           encode_aux 0 ((Many (count + 1, x)) :: acc) xs
  in List.rev (encode_aux 0 [] l)

