(* Problem 15: Replicate the Elements of a List a Given Number of Times *)

let replicate l n =
  if n <= 1 then l
  else
    let rec rep elem n acc =
      if n = 0 then acc
      else rep elem (n - 1) (elem :: acc)
    in
    let rec replicate_aux acc = function
      | [] -> acc
      | x :: xs -> replicate_aux (rep x n acc) xs
    in List.rev (replicate_aux [] l)
