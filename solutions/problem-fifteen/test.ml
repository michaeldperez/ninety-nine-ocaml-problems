open OUnit2
open Replicate

let tests = "Test suite for replicate the elements of a list given a number of times" >::: [
  "times less than or equal to 1" >:: (fun _ -> assert_equal ["a"; "b"] (replicate ["a"; "b"] 0));
  "empty" >:: (fun _ -> assert_equal [] (replicate [] 5));
  "multi-element" >:: (fun _ -> assert_equal ["a"; "a"; "a"; "b"; "b"; "b"; "c"; "c"; "c"] (replicate ["a"; "b"; "c"] 3));
]

let _ = run_test_tt_main tests

