open OUnit2
open Extract

let tests = "Test suite for generate the combination s of K distinct objects chosen from N elements of a list" >::: [
  "empty" >:: (fun _ -> assert_equal [] (extract 2 []));
  "n = 0" >:: (fun _ -> assert_equal [[]] (extract 0 ["a"; "b"; "c"; "d"]));
  "n > 0" >:: (fun _ -> assert_equal [["a"; "b"]; ["a"; "c"]; ["a"; "d"]; ["b"; "c"]; ["b"; "d"]; ["c"; "d"]] (extract 2 ["a"; "b"; "c"; "d"]));
]

let _ = run_test_tt_main tests

