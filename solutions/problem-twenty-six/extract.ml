(* Problem 26: Generate the Combinations of K Distinct Objects Chosen from the N Elements of a List *)

let rec extract n lst =
  if n <= 0 then [[]]
  else match lst with
  | [] -> []
  | x :: xs ->
      let head = List.map (fun l -> x :: l) @@ extract (n - 1) xs in
      let tail = extract n xs in
      head @ tail

