(* Problem 21: Insert and Element at a Given Position Into a List *)

let rec insert_at item index = function
  | [] -> [item]
  | x :: xs as t ->
      if index = 0 then item :: t 
      else x :: insert_at item (index - 1) xs 

