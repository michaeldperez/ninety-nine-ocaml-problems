open OUnit2
open Insert_at

let tests = "Test suite for insert an element at a given position into a list" >::: [
  "empty" >:: (fun _ -> assert_equal ["a"] (insert_at "a" 2 []));
  "first" >:: (fun _ -> assert_equal ["x"; "a"; "b"; "c"] (insert_at "x" 0 ["a"; "b"; "c"]));
  "index >= length" >:: (fun _ -> assert_equal ["a"; "b"; "c"; "x"] (insert_at "x" 4 ["a"; "b"; "c"]));
  "index in bounds" >:: (fun _ -> assert_equal ["a"; "alfa"; "b"; "c"; "d"] (insert_at "alfa" 1 ["a"; "b"; "c"; "d"]));
]

let _ = run_test_tt_main tests

