open OUnit2
open Drop

let tests = "Test suite for drop every nth element from a list" >::: [
  "n <= 0" >:: (fun _ -> assert_equal ["a"; "b"; "c"] (drop ["a"; "b"; "c"] 0));
  "n > list length" >:: (fun _ -> assert_equal ["a"; "b"; "c"] (drop ["a"; "b"; "c"] 4));
  "empty" >:: (fun _ -> assert_equal [] (drop [] 4));
  "in bounds" >:: (fun _ -> assert_equal ["a"; "b"; "d"; "e"; "g"; "h"; "j"] (drop ["a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"; "i"; "j"] 3));
]

let _ = run_test_tt_main tests

