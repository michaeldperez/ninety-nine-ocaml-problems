(* Problem 16: Drop Every Nth Element From a List *)

let drop l n =
  let rec drop_aux m acc = function
    | [] -> acc
    | x :: xs ->
      if m = 1 then drop_aux n acc xs
      else drop_aux (m - 1) (x :: acc) xs
  in List.rev (drop_aux n [] l)
