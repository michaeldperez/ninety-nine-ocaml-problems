open OUnit2
open Last_two

let tests = "Test suite for last_two" >::: [
    "emtpy" >:: (fun _ -> assert_equal None (last_two []));
    "singleton" >:: (fun _ -> assert_equal None (last_two [1]));
    "two elements" >:: (fun _ -> assert_equal (Some ("a", "b")) (last_two ["a"; "b"]));
    "multiple elements" >:: (fun _ -> assert_equal (Some ("c", "d")) (last_two ["a"; "b"; "c"; "d"]));
]

let _ = run_test_tt_main tests
