(* Problem 02: Last Two *)

let rec last_two = function
  | [x ; y] -> Some(x, y)
  | _ :: xs -> last_two xs
  | _ -> None
 

