(* Problem 08: Eliminate Duplicates *)

let compress list =
  if List.length list = 0 then []
  else
    let rec compress_aux acc cur = function
      | [] -> cur :: acc
      | x :: xs ->
        if x = cur then
          compress_aux acc cur xs
        else
          compress_aux (cur :: acc) x xs
    in List.rev (compress_aux [] (List.hd list) list)

