open OUnit2
open Compress

let tests = "Test suite for eliminate duplicates" >::: [
    "empty" >:: (fun _ -> assert_equal [] (compress []));
    "single element" >:: (fun _ -> assert_equal [1] (compress [1]));
    "multi-element" >:: (fun _ -> assert_equal ["a"; "b"; "c"; "a"; "d"; "e"] (compress ["a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e"]));
]

let _ = run_test_tt_main tests
