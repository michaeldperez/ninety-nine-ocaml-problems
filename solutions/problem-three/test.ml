open OUnit2
open Nth

let tests = "Test suite for Nth" >::: [
    "negative n" >:: (fun _ -> assert_raises (Failure "nth") (fun () -> nth ["a"] (-1)));
    "empty list" >:: (fun _ -> assert_raises (Failure "nth") (fun () -> nth [] 2));
    "out of bounds" >:: (fun _ -> assert_raises (Failure "nth") (fun () -> nth ["a"] 2));
    "in bounds" >:: (fun _ -> assert_equal "c" (nth ["a"; "b"; "c"; "d"; "e"] 2));
]

let _ = run_test_tt_main tests
