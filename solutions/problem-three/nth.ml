(* Problem 03: Nth *)

let rec nth l n =
  if n < 0 then raise (Failure "nth")
  else match (l, n) with
    | ([], _) -> raise (Failure "nth")
    | (x::_, 0) -> x
    | (_::xs, m) -> nth xs (m - 1)
