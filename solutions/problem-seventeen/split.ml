(* Problem 17: Split a List Into Two Parts; The Length of the First Part is Given *)

let split l n =
  let rec split_aux acc l n =
    match (l, n) with
      | ([], _) -> (List.rev acc, l)
      | (xs, 0) -> (List.rev acc, xs)
      | (x::xs, m) -> split_aux (x :: acc) xs (m - 1)
  in split_aux [] l n

