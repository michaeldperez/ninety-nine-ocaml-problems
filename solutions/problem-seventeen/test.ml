open OUnit2
open Split

let tests = "Test suite for split a list into two parts; the length of the first part is given" >::: [
  "split on 0" >:: (fun _ -> assert_equal ([], ["a"; "b"; "c"]) (split ["a"; "b"; "c"] 0));
  "split on n > length" >:: (fun _ -> assert_equal (["a"; "b"; "c"; "d"], []) (split ["a"; "b"; "c"; "d"] 5));
  "split on n < length" >:: (fun _ -> assert_equal (["a"; "b"; "c"; "d"], []) (split ["a"; "b"; "c"; "d"] (-5)));
  "split in bounds" >:: (fun _ -> assert_equal (["a"; "b"; "c"], ["d"; "e"; "f"; "g"; "h"; "i"; "j"]) (split ["a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"; "i"; "j"] 3));
]

let _ = run_test_tt_main tests

