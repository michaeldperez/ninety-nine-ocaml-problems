(* Problem 25: Generate a Random Permutation of the Elements of a List *)

let permutation l =
  let rec pluck acc idx = function
    | [] -> raise Not_found
    | x :: xs ->
        if idx = 0 then (x, acc @ xs)
        else pluck (x :: acc) (idx - 1) xs
  in
  let remove_random_elem len lst =
    pluck [] (Random.int len) lst
  in
  let rec aux acc len lst = 
    if len = 0 then acc
    else
      let elem, rest = remove_random_elem len lst in
        aux (elem :: acc) (len - 1) rest
  in aux [] (List.length l) l

