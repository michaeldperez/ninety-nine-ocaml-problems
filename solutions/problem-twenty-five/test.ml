open OUnit2
open Permutation

let tests = "Test suite for generate a random permutation of the elements of a list" >::: [
  "random permutation" >:: (fun _ -> assert_equal ["c"; "d"; "f"; "e"; "b"; "a"]  (permutation ["a"; "b"; "c"; "d"; "e"; "f"]));
]

let _ = run_test_tt_main tests

