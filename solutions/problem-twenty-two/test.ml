open OUnit2
open Range

let tests = "Test suite for TODO" >::: [
  "start = finish" >:: (fun _ -> assert_equal [4] (range 4 4));
  "start < finish" >:: (fun _ -> assert_equal [4; 5; 6; 7; 8; 9] (range 4 9));
  "start > finish" >:: (fun _ -> assert_equal [9; 8; 7; 6; 5; 4] (range 9 4));
]

let _ = run_test_tt_main tests

