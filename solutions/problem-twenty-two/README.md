# Create a List Containing All Integers Within a Given Range

If the first argument is greater than the second, produce a list in decreasing order.

```ocaml
# range 4 9;;
- : list int = [4; 5; 6; 7; 8; 9]
```

