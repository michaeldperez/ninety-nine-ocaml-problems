(* Problem 22: Create a List Containing All Integers Within a Given Range *)

let range start finish =
  let inc = if start <= finish then 1 else -1 in
    let rec range_aux s f =
      if s = f then [s]
      else s :: range_aux (s + inc) f
    in range_aux start finish
