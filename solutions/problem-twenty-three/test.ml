open OUnit2
open Rand_select

let tests = "Test suite for TODO" >::: [
  "random 3" >:: (fun _ -> (Random.init 99); assert_equal ["a"; "h"; "g"]  (rand_select ["a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"] 3));
  "Invalid Arg" >:: (fun _ -> assert_raises (Invalid_argument "rand_select") (fun () -> rand_select ["a"; "b"; "c"] (-1)));
]

let _ = run_test_tt_main tests

