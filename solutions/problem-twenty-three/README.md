# Extract a Given Number of Randomly Selected Elements From a List

The selected items shall be returned in a list. We use the **Random** module but do not initialize it with **Random.self_init** for reproducibility.

```ocaml
# rand_select ["a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"] 3;;
- : string list = ["g"; "d"; "a"]
```

