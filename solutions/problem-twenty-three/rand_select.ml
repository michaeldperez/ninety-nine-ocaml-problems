(* Problem 23: Extract a Given Number of Randomly Selected elements From a List *)

let rand_select l n =
  if n < 0 then raise (Invalid_argument "rand_select")
  else
    let rec at i = function
      | [] -> raise (Failure "rand_select")
      | x :: xs -> if i = 0 then x else at (i - 1) xs
    in
    let rec rand_select_aux n acc l = 
      if n = 0 then acc
      else rand_select_aux (n - 1) ((at (Random.int (List.length l)) l) :: acc) l 
    in rand_select_aux n [] l

