open OUnit2
open Pack

let tests = "Test suite for pack consecutive duplicates" >::: [
    "empty" >:: (fun _ -> assert_equal [] (pack []));
    "single element" >:: (fun _ -> assert_equal [["a"]] (pack ["a"]));
    "unique elements" >:: (fun _ -> assert_equal [["a"]; ["b"]; ["c"]] (pack ["a"; "b"; "c"]));
    "duplicates" >:: (fun _ -> assert_equal [["a"; "a"; "a"; "a"]; ["b"]; ["c"; "c"]; ["a"; "a"]; ["d"; "d"]; ["e"; "e"; "e"; "e"]] (pack ["a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "d"; "e"; "e"; "e"; "e"]));
]

let _ = run_test_tt_main tests
