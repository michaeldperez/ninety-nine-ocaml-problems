(* Problem 09: Pack Consecutive Duplicates *)

let pack l =
  let rec pack_aux sacc bacc = function
    | [] -> bacc
    | [x] -> bacc @ [x :: sacc]
    | x :: (y :: _ as xs) ->
      if x = y then pack_aux (x :: sacc) bacc xs
      else pack_aux [] (bacc @ [x :: sacc]) xs
  in pack_aux [] [] l
