(* Problem 05: Reverse a List *)

let rev l =
  let rec rev_aux r = function
    | [] -> r
    | x :: xs -> rev_aux (x :: r) xs
  in
  rev_aux [] l
