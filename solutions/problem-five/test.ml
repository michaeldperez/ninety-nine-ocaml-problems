open OUnit2
open Rev

let tests = "Test suite for rev" >::: [
    "empty" >:: (fun _ -> assert_equal [] (rev []));
    "non-empty" >:: (fun _ -> assert_equal ["c"; "b"; "a"] (rev ["a"; "b"; "c"]));
]

let _ = run_test_tt_main tests
