open OUnit2
open Encode

let tests = "Test suite for run-length encoding" >::: [
    "empty" >:: (fun _ -> assert_equal [] (encode []));
    "single element" >:: (fun _ -> assert_equal [(1, "a")] (encode ["a"]));
    "multi-element" >:: (fun _ -> assert_equal [(4, "a"); (1, "b"); (2, "c"); (2, "a"); (1, "d"); (4, "e")] (encode ["a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e"]));
]

let _ = run_test_tt_main tests
