(* Problem 10: Run-Length Encoding *)

let encode l =
  let rec encode_aux count acc = function
    | [] -> acc
    | [x] -> (count + 1, x) :: acc
    | x :: (y :: _ as xs) ->
      if x = y then encode_aux (count + 1) acc xs
      else encode_aux 0 ((count + 1, x) :: acc) xs
  in List.rev (encode_aux 0 [] l)
