(* Problem 13: Run-Length Encoding of a List (Direct Solution) *)

type 'a rle =
  | One of 'a
  | Many of int * 'a

let encode l =
  let many count e =
    if count = 0 then One e else Many (count + 1, e)
  in
  let rec encode_aux count acc = function
    | [] -> []
    | [x] -> (many count x) :: acc
    | x :: (y :: _ as xs) ->
        if x = y then encode_aux (count + 1) acc xs
        else encode_aux 0 ((many count x) :: acc) xs
  in List.rev (encode_aux 0 [] l)

