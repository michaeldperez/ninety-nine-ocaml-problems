# Decode a Run-Length Encoded List

Given a run-length code list generated as specified in the previous problem, construct it's uncompressed version.

```ocaml
# decode [Many (4, "a"); One "b"; "Many (2, "c"); Many (2, "a"); One "d"; Many (4, "e")];;
- : string list = ["a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e"]
```

