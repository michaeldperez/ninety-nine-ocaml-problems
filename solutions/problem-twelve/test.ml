open OUnit2
open Decode

let tests = "Test suite for decode a run-length encoded list" >::: [
  "empty" >:: (fun _ -> assert_equal [] (decode []));
  "one element" >:: (fun _ -> assert_equal ["a"] (decode [One "a"]));
  "unique elements" >:: (fun _ -> assert_equal ["a"; "b"; "c"] (decode [One "a"; One "b"; One "c"]));
  "multi-element" >:: (fun _ -> assert_equal ["a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e"]
    (decode [Many (4, "a"); One "b"; Many (2, "c"); Many (2, "a"); One "d"; Many (4, "e")]));
]

let _ = run_test_tt_main tests

