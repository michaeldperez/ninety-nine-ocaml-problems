(* Problem 12: Decode a Run-Length Encoded List *)

type 'a rle =
  | One of 'a
  | Many of int * 'a

let rec decode = function
  | [] -> []
  | One x :: xs -> x :: decode xs
  | Many (1, x) :: xs -> x :: decode xs
  | Many (n, x) :: xs -> x :: decode (Many (n - 1, x) :: xs) 

