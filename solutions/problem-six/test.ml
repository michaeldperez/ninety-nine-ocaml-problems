open OUnit2
open Palindrome

let tests = "Test suite for palindrome" >::: [
    "empty" >:: (fun _ -> assert_equal true (is_palindrome []));
    "palindrome" >:: (fun _ -> assert_equal true (is_palindrome ["r"; "a"; "c"; "e"; "c"; "a"; "r"]));
    "not palindrome" >:: (fun _ -> assert_equal false (is_palindrome ["a"; "b"]));
]

let _ = run_test_tt_main tests
