(* Problem 06: Palindrome *)

let is_palindrome l =
  l = List.rev l
