open OUnit2
open Flatten

let tests = "Test suite for flatten" >::: [
    "flattens nested node structure" >:: (fun _ -> assert_equal ["a"; "b"; "c"; "d"; "e"] (flatten [One "a"; Many [One "b"; Many [One "c" ;One "d"]; One "e"]]));
]

let _ = run_test_tt_main tests
