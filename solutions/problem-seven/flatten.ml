(* Problem 07: Flatten a List *)

type 'a node =
  | One of 'a
  | Many of 'a node list

let rec flatten = function
  | [] -> []
  | One x :: xs -> x :: (flatten xs)
  | Many t :: xs -> (flatten t) @ (flatten xs)
