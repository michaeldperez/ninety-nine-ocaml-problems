open OUnit2
open Lotto_select

let tests = "Test suite for lotto: draw N different random numbers from the set 1..M" >::: [
  "lotto" >:: (fun _ -> (Random.init 99); assert_equal [48; 8; 11; 47; 46; 38] (lotto_select 6 49));
  "zero" >:: (fun _ -> (Random.init 99); assert_equal [] (lotto_select 0 49));
]

let _ = run_test_tt_main tests

