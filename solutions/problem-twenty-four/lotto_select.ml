(* Problem 24: Lotto - Draw N Different Random Numbers From the Set 1..M *)

let rec lotto_select n m =
  if n = 0 then []
  else (Random.int m) :: lotto_select (n - 1) m
