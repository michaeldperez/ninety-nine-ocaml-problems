# Lotto: Draw N Different Random Numbers From the Set 1..M

Drawn N different random numbers from the set `1..M`.

The selected numbers shall be returned in a list.

```ocaml
# lotto_select 6 49;;
- : int list = [20; 28; 45; 16; 24; 38]
```

