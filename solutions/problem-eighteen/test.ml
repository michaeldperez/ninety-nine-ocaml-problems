open OUnit2
open Slice

let tests = "Test suite for extract a slice from a list" >::: [
  "empty" >:: (fun _ -> assert_equal [] (slice [] 2 3));
  "in bounds" >:: (fun _ -> assert_equal ["c"; "d"; "e"; "f"; "g"] (slice ["a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"; "i"; "j"] 2 6));
  "out bounds (upper)" >:: (fun _ -> assert_equal ["a"; "b"; "c"] (slice ["a"; "b"; "c"] 0 5));
  "out bounds (lower)" >:: (fun _ -> assert_equal [] (slice ["a"; "b"; "c"] 13 5));
]

let _ = run_test_tt_main tests

