(* Problem 18: Extract a Slice From a List *)

let slice l i j =
  let rec drop n = function
    | [] -> []
    | _ :: xs as t ->
        if n = 0 then t
        else drop (n - 1) xs
  in
  let rec take n acc = function
    | [] -> acc
    | x :: xs ->
        if n = 0 then x :: acc
        else take (n - 1) (x :: acc) xs
  in List.rev (take (j - i) [] (drop i l))

