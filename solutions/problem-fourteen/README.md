# Duplicate the Elements of a List

Duplicate the elements of a list.

```ocaml
# duplicate ["a"; "b"; "c"; "c"; "d"];;
- : string list = ["a"; "b"; "c"; "c"; "c"; "c"; "d"; "d"]
```

