open OUnit2
open Duplicate

let tests = "Test suite for duplicate" >::: [
  "empty" >:: (fun _ -> assert_equal []  (duplicate []));
  "single element" >:: (fun _ -> assert_equal ["a"; "a"]  (duplicate ["a"]));
  "multi-element" >:: (fun _ -> assert_equal ["a"; "a"; "b"; "b"; "c"; "c"; "c"; "c"; "d"; "d"] (duplicate ["a"; "b"; "c"; "c"; "d"]));
]

let _ = run_test_tt_main tests

