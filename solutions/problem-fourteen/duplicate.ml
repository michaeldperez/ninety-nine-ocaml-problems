(* Problem 14: Duplicate the Elements of a List *)

let rec duplicate = function
  | [] -> []
  | x :: xs -> x :: x :: duplicate xs

