open OUnit2
open Last

let tests = "Test suite for last" >::: [
    "empty" >:: (fun _-> assert_equal None (last []));
    "singleton" >:: (fun _-> assert_equal (Some 1) (last [1]));
    "list" >:: (fun _-> assert_equal (Some "d") (last ["a"; "b"; "c"; "d"]));
]

let _ = run_test_tt_main tests
