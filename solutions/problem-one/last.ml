(* Problem 01: Last *)

let rec last = function
  | [] -> None
  | [x] -> Some x
  | _ :: t -> last t
